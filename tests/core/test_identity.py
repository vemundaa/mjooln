import pytest

from mjooln import *


def test_identity():
    id1 = Identity()
    id2 = Identity(str(id1))
    assert id1 == id2
    id1 = Identity()
    id2 = Identity()
    id3 = Identity()
    assert Identity(str(id1)) == id1
    idstr = f'Some {id1} thing with {id2} identities {id3} spread in between'
    assert Identity.find_seeds(idstr) == [id1, id2, id3]
    with pytest.raises(BadSeed):
        id1 = Identity.find_seed('No id here')
    assert Identity.seed_in(f'One id {id1} here')
    assert not Identity.seed_in('No id here')


def test_not_identity():
    with pytest.raises(IdentityError):
        Identity('Not an identity')


def test_classic_uuid():
    classic = 'b92f736b-c59f-487c-861b-92a788cc385a'
    identity = Identity.from_classic(classic)
    assert identity == classic.replace('-', '_').upper()
    assert identity.classic() == classic
    compact = classic.replace('-', '')
    identity = Identity.from_compact(compact)
    assert identity == classic.replace('-', '_').upper()
    assert identity.compact() == compact


def test_is_classic():
    classic = 'b92f736b-c59f-487c-861b-92a788cc385a'
    assert Identity.is_classic(classic) is True
    assert Identity.is_classic(str(Identity())) is False
    classic_compact = 'b92f736bc59f487c861b92a788cc385a'
    assert Identity.is_compact(classic_compact) is True
    assert Identity.is_compact(str(Identity())) is False


def test_is_seed():
    id_str = 'B92F736B_C59F_487C_861B_92A788CC385A'
    assert Identity.is_seed(id_str)
    assert not Identity.is_seed('No identity here')
    not_id_str = 'B92F7_6B_C59F_487C_861B_92A788CC385A'
    assert not Identity.is_seed(not_id_str)
    not_id_str = 'B92F736B_C59F_487C_861B_92A788CC385AA'
    assert not Identity.is_seed(not_id_str)
    not_id_str = 'B92F736B_C59F_487C_861B_92A788cC385A'
    assert not Identity.is_seed(not_id_str)
    not_id_str = 'B9F736B_C59F_487C_861B_92A788CC385A'
    assert not Identity.is_seed(not_id_str)
    not_id_str = 'B92F736B_C59F_487C861B_92A788CC385A'
    assert not Identity.is_seed(not_id_str)


def test_elf():
    id_str = str(Identity())
    id_ish = id_str.lower().replace('_', '')
    assert id_str == Identity.elf(id_ish)
    id_ish = id_str.lower().replace('_', '-')
    assert id_str == Identity.elf(id_ish)
    id_ish = f'some {id_str} id inside'
    assert id_str == Identity.elf(id_ish)
    id_ish = f'some {id_str} id inside {id_str}'
    assert id_str == Identity.elf(id_ish)
    id_ish = f'some{id_str}id inside'
    assert id_str == Identity.elf(id_ish)


def test_multiple_identities_in_string():
    ids = [Identity() for _ in range(3)]
    assert len(ids) == 3
    multiple_ids = f'some id {ids[0]} another one ' \
                   f'{ids[1]} and a last one {ids[2]}'
    assert ids == Identity.find_seeds(multiple_ids)
    # Check that elf finds and picks the first one
    assert ids[0] == Identity.elf(multiple_ids)

    assert [] == Identity.find_seeds('Some string with no Identities in it '
                                     'anywhere to be found')
