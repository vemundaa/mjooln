import binascii

import pytest

from mjooln import *


def test_error_on_string():
    crypt_key = Crypt.generate_key()
    with pytest.raises(TypeError):
        Crypt.encrypt('not_bytes', crypt_key)

    with pytest.raises(CryptError):
        Crypt.decrypt('not_bytes', crypt_key)


def test_key_from_password():
    salt = b'xCKxV-Jq1bUZCOWHwLckTLga'
    pw = 'a very bad password'
    expected_crypt_key = b'I1UJXeKCo2XeLYJJdNvDd5PqvfTuNxRaBtVkAlAjUF4='
    crypt_key = Crypt.key_from_password(salt, pw)
    assert crypt_key == expected_crypt_key


def test_encrypt_decrypt():
    crypt_key = b'I1UJXeKCo2XeLYJJdNvDd5PqvfTuNxRaBtVkAlAjUF4='
    text_in = 'Some random text to encrypt with our new and salty password'
    data_in = text_in.encode()
    text_aes = Crypt.encrypt(data_in, crypt_key)
    data_out = Crypt.decrypt(text_aes, crypt_key)
    text_out = data_out.decode()
    assert text_in == text_out

