import pytest

from mjooln import *


def test_atom():
    k = Key('some_key')
    z = Zulu()
    i = Identity()
    s = Atom(k, zulu=z, identity=i)
    assert str(s) == f'{str(z)}{CLASS_SEPARATOR}{str(k)}' \
                     f'{CLASS_SEPARATOR}{str(i)}'
    assert Atom.is_seed(str(s))
    assert s.key() == k
    assert s.zulu() == z
    assert s.identity() == i
    ss = Atom.from_seed(str(s))
    assert str(ss) == str(s)

    with pytest.raises(AtomError):
        Atom(str(s))
    with pytest.raises(TypeError):
        Atom(zulu=z)
    with pytest.raises(TypeError):
        Atom(zulu=z, identity=i)

    with pytest.raises(AtomError):
        Atom(f'{z}{k}{CLASS_SEPARATOR}{i}')

    with pytest.raises(AtomError):
        Atom(f'{z}{CLASS_SEPARATOR}{k}{i}')

    with pytest.raises(AtomError):
        Atom('This is not at all an atom')


def test_hashable():
    atom = Atom('test')
    assert hash(atom)

    dic = {
        atom: 'test'
    }
    assert dic[atom] == 'test'


def test_sort():
    a1 = Atom('test', zulu=Zulu(2020, 1, 1))
    a2 = Atom('test', zulu=Zulu(2020, 1, 2))
    a3 = Atom('test', zulu=Zulu(2020, 1, 3))

    ordered = [a1, a2, a3]
    unordered = [a3, a1, a2]

    assert ordered != unordered
    sorted = unordered
    sorted.sort()
    assert ordered == sorted

    a1 = Atom('test_a')
    a2 = Atom('test_b')
    a3 = Atom('test_c')

    ordered = [a1, a2, a3]
    unordered = [a3, a1, a2]

    assert ordered != unordered
    sorted = unordered
    sorted.sort()
    assert ordered == sorted


def test_atom_doc():
    a = Atom('test_me')
    dic = a.to_dict()
    doc = a.to_doc()
    assert dic['identity'] == doc['identity']
    assert dic['zulu'] == a.zulu()
    assert dic['key'] == a.key()
    assert doc['zulu'] == a.zulu().iso()
    assert doc['key'] == str(a.key())
    assert doc['identity'] == str(a.identity())

    b = Atom.from_doc(doc)
    assert a == b
    c = Atom(**dic)
    assert a == c

    a = Atom('test_yaml')
    ay = a.to_yaml()
    b = Atom.from_yaml(ay)
    assert a == b

    a = Atom('test_json')
    aj = a.to_json()
    b = Atom.from_json(aj)
    assert a == b


def test_is_atom():
    astr = '20200824T114429u214542Z___test___' \
           '12B6F700_7D40_4EEB_B688_3389AB95AC53'
    assert Atom.is_seed(astr)
    # TODO: Find a way to make this rejected with regex
    # not_astr = '20200824T114429u214542Z___test___not___' \
    #        '12B6F700_7D40_4EEB_B688_3389AB95AC53'
    # assert not Atom.is_seed(not_astr)
    not_astr = '20200824T114429u214542Z___test___' \
               '12B6F00_7D40_4EEB_B688_3389AB95AC53'
    assert not Atom.is_seed(not_astr)
    not_astr = '20200824T11442u214542Z___test___' \
               '12B6F700_7D40_4EEB_B688_3389AB95AC53___test'
    assert not Atom.is_seed(not_astr)
    not_astr = '20200824T114429u214542Z__test___' \
               '12B6F700_7D40_4EEB_B688_3389AB95AC53'
    assert not Atom.is_seed(not_astr)
    not_astr = '20200824T114429u214542Z___test__' \
               '12B6F700_7D40_4EEB_B688_3389AB95AC53'
    assert not Atom.is_seed(not_astr)

    not_astr = '20200824T114429u214542Z'
    assert not Atom.is_seed(not_astr)
    not_astr = 'test'
    assert not Atom.is_seed(not_astr)
    not_astr = '12B6F700_7D40_4EEB_B688_3389AB95AC53'
    assert not Atom.is_seed(not_astr)


def test_elf():
    # TODO: More tests
    a = Atom.elf('test')
    b = Atom('test')
    assert a.key() == b.key()
    assert a.zulu().epoch() == pytest.approx(b.zulu().epoch())
    c = Atom.elf(str(b))
    assert b == c
