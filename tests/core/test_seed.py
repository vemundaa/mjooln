import pytest

from mjooln import *


@pytest.fixture()
def sample():
    class SampleSeed(str, Seed):
        REGEX = r'\d'

        @classmethod
        def from_seed(cls, str_: str):
            return cls(str_)

    return SampleSeed


def test_is(sample):
    assert not sample.is_seed('a')
    assert not sample.is_seed('aa')
    assert sample.is_seed('5')
    assert not sample.is_seed('44')
    assert not sample.is_seed('b4')
    assert not sample.is_seed('4b')


def test_seed_in(sample):
    assert not sample.seed_in('a')
    assert not sample.seed_in('aa')
    assert sample.seed_in('5')
    assert sample.seed_in('44')
    assert sample.seed_in('b4')
    assert sample.seed_in('4b')


def test_find_seed(sample):
    with pytest.raises(BadSeed):
        sample.find_seed('a')
    with pytest.raises(BadSeed):
        sample.find_seed('aa')
    assert sample.find_seed('5')
    assert sample.find_seed('a4')
    assert sample.find_seed('aa4')
    assert sample.find_seed('aa4a')


def test_find_seeds(sample):
    assert sample.find_seeds('a') == []
    assert sample.find_seeds('aa') == []
    assert sample.find_seeds('5') == ['5']
    assert sample.find_seeds('a4') == ['4']
    assert sample.find_seeds('aa4') == ['4']
    assert sample.find_seeds('aa4a') == ['4']
    assert sample.find_seeds('a3a4a') == ['3', '4']
    assert sample.find_seeds('a3a4a5') == ['3', '4', '5']
    assert sample.find_seeds('2a3a4a5') == ['2', '3', '4', '5']
