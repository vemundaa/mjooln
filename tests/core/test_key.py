import pytest

from mjooln import *


def test_key():
    key = Key('some_key')
    assert key == 'some_key'
    if PIXIE:
        with pytest.raises(PixieInPipeline):
            Key('Invalidkey')
        with pytest.raises(PixieInPipeline):
            Key('invalid key')
        with pytest.raises(PixieInPipeline):
            Key('3invalidkey')
        with pytest.raises(PixieInPipeline):
            Key('_invalidkey')
        with pytest.raises(PixieInPipeline):
            Key('invalid___separator')
    else:
        with pytest.raises(InvalidKey):
            Key('Invalidkey')
        with pytest.raises(InvalidKey):
            Key('invalid key')
        with pytest.raises(InvalidKey):
            Key('3invalidkey')
        with pytest.raises(InvalidKey):
            Key('_invalidkey')
        with pytest.raises(InvalidKey):
            Key('invalid___separator')

    assert ['aaa', 'bbb', 'ccc'] == Key('aaa__bbb__ccc').words()
    assert ['a_b', 'c_d', 'e_f'] == Key('a_b__c_d__e_f').words()


def test_is_seed():
    assert Key.is_seed('valid_key')
    assert Key.is_seed('valid__key__for_real')
    assert not Key.is_seed('_invalid_key__at_all')
    assert not Key.is_seed('__invalid_key__at_all')
    assert not Key.is_seed('at_all__invalid_key_')
    assert not Key.is_seed('__at_allinvalid_key__')
    assert not Key.is_seed('2invalid_key__at_all')
    assert not Key.is_seed('invalid key__at_all')
    assert Key.is_seed('valid__key__no2')
    assert Key.is_seed('a__at_all')
    assert Key.is_seed('abc__at_all')
    assert not Key.is_seed('_ab__at_all')
    assert Key.is_seed('a_b__at_all')
    assert not Key.is_seed('at_all__ab_')
    assert not Key.is_seed('at_all__a__')
    assert not Key.is_seed('__a__at_all')


def test_elf():
    assert str(Key.elf('AbCd')) == 'abcd'
    assert str(Key.elf('AbC2d')) == 'abc2d'
    assert str(Key.elf('Ab.Cd')) == 'ab_cd'
    assert str(Key.elf('.Ab.Cd')) == 'ab_cd'
    assert str(Key.elf('..Ab.Cd')) == 'ab_cd'
    assert str(Key.elf('Ab.Cd.')) == 'ab_cd'
    assert str(Key.elf('Ab.Cd..')) == 'ab_cd'
    assert str(Key.elf('Ab..Cd.')) == 'ab__cd'

    assert str(Key.elf('AbCd__Cd')) == 'ab_cd__cd'

    assert str(Key.elf('just_testing')) == 'just_testing'


def test_words():
    wstrs = ['one_word', 'two_word', 'three_word']
    words = [Word(x) for x in wstrs]
    keystr = 'one_word__two_word__three_word'
    key = Key(keystr)
    assert Key.from_words('one_word', 'two_word', 'three_word') == key
    assert Key.from_words(words) == key
    assert Key.from_words(wstrs) == key
    assert Key.from_words(keystr) == key
    assert Key('one_word__two_word__three_word').first() == Word('one_word')
    assert Key('one_word__two_word__three_word').first() == 'one_word'
    assert Key('one_word__two_word__three_word').last() == 'three_word'


def test_parts():
    assert Key('one_word__two_word__three_word').parts() == ['one_word', 'two_word', 'three_word']
    assert Key('one_word__three_word').parts() == ['one_word', 'three_word']


def test_branch():
    assert Key('one_word__two_word__three_word').branch() == 'one_word/two_word/three_word'
    assert Key('one_word__three_word').branch() == 'one_word/three_word'
    assert Key('one_word__two_word__three_word') == Key.from_branch('one_word/two_word/three_word')
    assert Key('one_word__three_word') == Key.from_branch('one_word/three_word')


def test_add():
    assert Key('aa__bb') == Key('aa') + Key('bb')
    assert Key('aa__bb') == Key('aa') + Word('bb')
    assert Key('aa__bb') == Key('aa') + 'bb'

    assert Key('bb__aa') == Word('bb') + Key('aa')
    assert Key('bb__aa') == 'bb' + Key('aa')
