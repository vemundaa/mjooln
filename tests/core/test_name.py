from mjooln import *


def test_camel_to_snake():
    assert Text.camel_to_snake('ThisIsCamel') == 'this_is_camel'
    assert Text.camel_to_snake('AlsoCamel2') == 'also_camel2'
    assert Text.camel_to_snake('Very') == 'very'


def test_snake_to_camel():
    assert Text.snake_to_camel('this_is_snake') == 'ThisIsSnake'
    assert Text.snake_to_camel('this_snake') == 'ThisSnake'
    assert Text.snake_to_camel('this') == 'This'
