import pytest

from mjooln import *


def test_word():
    word = Word('some_word')
    assert word == 'some_word'
    if PIXIE:
        with pytest.raises(PixieInPipeline):
            Word('Invalidword')
        with pytest.raises(PixieInPipeline):
            Word('invalid word')
        with pytest.raises(PixieInPipeline):
            Word('3 invalidword')
        with pytest.raises(PixieInPipeline):
            Word('_invalidword')
        with pytest.raises(PixieInPipeline):
            Word('invalid__separator')
    else:
        with pytest.raises(BadWord):
            Word.check('Invalidword')
        with pytest.raises(BadWord):
            Word.check('invalid word')
        with pytest.raises(BadWord):
            Word.check('3 invalidword')
        with pytest.raises(BadWord):
            Word.check('_invalidword')
        with pytest.raises(BadWord):
            Word.check('invalid__separator')


def test_is_seed():
    assert Word.is_seed('valid_key')
    assert not Word.is_seed('invalid__word__for_real')
    assert not Word.is_seed('_invalid_word')
    assert not Word.is_seed('__invalid_word')
    assert not Word.is_seed('invalid_word_')
    assert not Word.is_seed('invalid_word__')
    assert Word.is_seed('2valid_word')
    assert not Word.is_seed('invalid word')
    assert not Word.is_seed('invalid__word__no2')
    assert Word.is_seed('valid_word_2')
    assert Word.is_seed('valid_2')
    assert Word.is_seed('valid2')
    assert Word.is_seed('2_valid')
    assert Word.is_seed('2valid')
    # Handle lower word lenghts specifically. Should never be more than 3
    if MINIMUM_WORD_LENGTH == 1:
        assert Word.is_seed('a')
    else:
        assert not Word.is_seed('a')
    if MINIMUM_WORD_LENGTH <= 2:
        assert Word.is_seed('ab')
    elif MINIMUM_WORD_LENGTH < 2:
        assert not Word.is_seed('ab')
    assert Word.is_seed('abc')
    assert not Word.is_seed('_ab')
    assert not Word.is_seed('_a')
    assert Word.is_seed('a_b')
    assert not Word.is_seed('ab_')
    assert not Word.is_seed('a__')
    assert not Word.is_seed('__a')
    assert Word.is_seed('12')
    assert Word.is_seed('1ab')
    assert Word.is_seed('a2b')
    assert Word.is_seed('ab3')
    assert Word.is_seed('a44')
    assert Word.is_seed('55a')


def test_integer():
    with pytest.raises(NotAnInteger):
        Word('not').to_int()
    with pytest.raises(NotAnInteger):
        Word('not_an').to_int()
    with pytest.raises(NotAnInteger):
        Word('not_an_integer').to_int()
    with pytest.raises(NotAnInteger):
        Word('not1').to_int()
    with pytest.raises(NotAnInteger):
        Word('not_an2').to_int()
    with pytest.raises(NotAnInteger):
        Word('not_an_integer3').to_int()
    with pytest.raises(NotAnInteger):
        Word('not_1').to_int()
    with pytest.raises(NotAnInteger):
        Word('not_an_2').to_int()
    with pytest.raises(NotAnInteger):
        Word('not_an_integer_3').to_int()
    with pytest.raises(NotAnInteger):
        Word('1not').to_int()
    with pytest.raises(NotAnInteger):
        Word('2not_an').to_int()
    with pytest.raises(NotAnInteger):
        Word('3not_an_integer').to_int()

    assert Word('33').to_int() == 33
    assert Word('123').to_int() == 123
    assert Word('142341234').to_int() == 142341234
    assert Word('1').to_int() == 1

    assert Word.from_int(3).to_int() == 3


def test_numeric():
    assert Word('22_33_66').is_numeric()
    assert Word('22_3366').is_numeric()
    assert Word('22').is_numeric()
    assert Word('22_33_66_333').is_numeric()
    assert not Word('22_3a3_66').is_numeric()
    assert not Word('22_a3366').is_numeric()
    assert not Word('22a').is_numeric()
    assert not Word('a22_33_66_333').is_numeric()

    assert Word('22_33_44').to_ints() == [22, 33, 44]
    assert Word.from_ints([22, 33, 44]).to_ints() == [22, 33, 44]


def test_increment():
    wi = Word('with_index_11')
    assert wi.index() == 11
    word = Word('some_word')
    assert word.index() == 0
    word1 = word.increment()
    word2 = word1.increment()
    assert str(word1) == 'some_word_1'
    assert str(word2) == 'some_word_2'
    assert word1 < word2
    for _ in range(100):
        word = word.increment()

    assert word == Word('some_word_100')


def test_equal():
    assert Word('a') == 'a'
    assert Word('b') == Word('b')


def test_add():
    assert Word('a') + 'b' == Word('a_b')
    assert 'b' + Word('a') == Word('b_a')
    assert Word('a') + Word('b') == Key('a__b')


def test_elf():
    assert Word.elf('test') == Word('test')
    assert Word.elf('Test') == Word('test')
    assert Word.elf('TestTest') == Word('test_test')
    assert Word.elf('Test_Test') == Word('test_test')
    assert Word.elf('Test.Test') == Word('test_test')
    assert Word.elf('Test..Test') == Word('test_test')
    assert Word.elf('Test./Test') == Word('test_test')
    assert Word.elf('.Test.Test') == Word('test_test')
    assert Word.elf('..Test..Test') == Word('test_test')
    assert Word.elf('Test./Test..') == Word('test_test')
    assert Word.elf(1) == Word('1')
    assert Word.elf(1.1) == Word('1_1')
    assert Word.elf(1e4) == Word('10000')
    with pytest.raises(AngryElf):
        Word.elf(Key('test'))
    with pytest.raises(AngryElf):
        Word.elf('test__key')
