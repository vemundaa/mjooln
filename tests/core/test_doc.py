from mjooln import *


class Doc1(Doc):

    def __init__(self,
                 a=3,
                 b=5.5,
                 c='text'):
        super(Doc1, self).__init__()
        self.a = a
        self.b = b
        self.c = c


class Doc2(Doc):

    def __init__(self,
                 a=3,
                 b=5.5,
                 c='text',
                 d=Doc1()):
        super(Doc2, self).__init__()
        self.a = a
        self.b = b
        self.c = c
        self.d = d

    def to_doc(self):
        doc = super().to_doc()
        doc['d'] = doc['d'].to_doc()
        return doc

    @classmethod
    def from_doc(cls, doc: dict):
        # Need custom method to handle special object. Not necessary
        # to do the same when converting to doc, since Doc objects are
        # converted automatically
        doc['d'] = Doc1.from_doc(doc['d'])
        return super(Doc2, cls).from_doc(doc)


class Doc3(Doc):
    def __init__(self,
                 zulu=Zulu(),
                 key=Key('some__key'),
                 identity=Identity(),
                 folder=Folder('~/somewhere/home')):
        super(Doc3, self).__init__()
        self.zulu = zulu
        self.key = key
        self.identity = identity
        self.folder = folder


def test_doc1():
    doc1 = Doc1()
    assert str(doc1) == 'Doc1(a=3, b=5.5, c=\'text\')'
    assert doc1.__repr__() == 'Doc1(a=3, b=5.5, c=\'text\')'
    data = doc1.to_dict()
    doc1b = doc1.from_dict(data)
    assert doc1b.to_dict() == doc1.to_dict()

    yaml_str = 'a: 3\nb: 5.5\nc: text\n'
    json_str = '{"a": 3, "b": 5.5, "c": "text"}'
    assert doc1.to_yaml() == yaml_str
    assert doc1.to_json() == json_str

    doc1c = Doc1.from_yaml(yaml_str)
    assert doc1c.to_dict() == doc1.to_dict()

    doc1d = Doc1.from_json(json_str)
    assert doc1d.to_dict() == doc1.to_dict()


def test_doc2():
    doc2 = Doc2()
    r = 'Doc2(a=3, b=5.5, c=\'text\', d=Doc1(a=3, b=5.5, c=\'text\'))'
    assert doc2.__repr__() == r
    assert str(doc2) == r
    data = doc2.to_dict()
    doc2b = doc2.from_dict(data)
    assert doc2b.to_dict() == doc2.to_dict()

    yaml_str = 'a: 3\nb: 5.5\nc: text\nd:\n  a: 3\n  b: 5.5\n  c: text\n'
    json_str = '{"a": 3, "b": 5.5, "c": "text", ' \
               '"d": {"a": 3, "b": 5.5, "c": "text"}}'
    assert doc2.to_yaml() == yaml_str
    assert doc2.to_json() == json_str

    doc2c = Doc2.from_yaml(yaml_str)
    assert doc2c.to_yaml() == doc2.to_yaml()
    assert doc2c.to_json() == doc2.to_json()

    doc2d = Doc2.from_json(json_str)
    assert doc2d.to_yaml() == doc2.to_yaml()
    assert doc2d.to_json() == doc2.to_json()


def test_json_list():
    some_list = [
        {'one': 1}, {'one': 2}, {'two': 2}
    ]
    json = JSON.dumps(some_list)
    dic = JSON.loads(json)
    assert dic == some_list


def test_zulu_and_seed_and_path_in_doc():
    doc3 = Doc3()
    doc = doc3.to_doc()
    if ZULU_TO_ISO:
        assert doc['zulu'] == doc3.zulu.iso()
    else:
        assert doc['zulu'] == doc3.zulu.seed()
    assert doc['key'] == doc3.key.seed()
    assert doc['key'] == str(doc3.key)
    assert doc['identity'] == doc3.identity.seed()
    assert doc['folder'] == str(doc3.folder)
