import pytest

from mjooln import *

samples = [
    ('Zulu(2020, 1, 1)',
     '20200101T000000u000000Z',
     '2020-01-01T00:00:00+00:00',
     '2020-01-01T00:00:00.000000+00:00'),

    ('Zulu(2020, 1, 1, 12, 12, 12)',
     '20200101T121212u000000Z',
     '2020-01-01T12:12:12+00:00',
     '2020-01-01T12:12:12.000000+00:00'
     ),

    ('Zulu(2020, 1, 1, 12, 12, 12, 30000)',
     '20200101T121212u030000Z',
     '2020-01-01T12:12:12.030000+00:00',
     '2020-01-01T12:12:12.030000+00:00'),

    ('Zulu(2020, 10, 6, 11, 7, 37, 512216)',
     '20201006T110737u512216Z',
     '2020-10-06T11:07:37.512216+00:00',
     '2020-10-06T11:07:37.512216+00:00'),
]


def test_constructor_numbers():
    z = Zulu(2020, 1, 1)
    assert str(z) == '20200101T000000u000000Z'
    z = Zulu(2020, 1, 1, 20, 20, 20)
    assert str(z) == '20200101T202020u000000Z'
    z = Zulu(2020, 1, 1, 20, 20, 20, 123456)
    assert str(z) == '20200101T202020u123456Z'
    with pytest.raises(ZuluError):
        # Insufficient constructor (elf would work)
        Zulu(2020)
    with pytest.raises(ZuluError):
        # Insufficient constructor (elf would work)
        Zulu(2020, 1)


def test_constructor_invalid_inputs():
    z = Zulu.now()
    with pytest.raises(ZuluError):
        Zulu(z)
    with pytest.raises(ZuluError):
        Zulu(z.seed())
    with pytest.raises(ZuluError):
        Zulu(z.iso())
    with pytest.raises(ZuluError):
        Zulu('some_string')


def test_constructor_datetime():
    a = datetime.datetime.now()
    with pytest.raises(ZuluError):
        # Constructor does not allow datetime without tzinfo
        Zulu(a)
    b = a.replace(tzinfo=pytz.utc)
    c = Zulu(b)
    assert c.hour == a.hour
    assert c.second == a.second

    dt = c.to_local()
    dtt = c.to_tz('Pacific/Easter')

    assert str(c) == str(Zulu(dt))
    assert str(c) == str(Zulu(dtt))


def test_fixed_iso_str():
    zulu = Zulu()


def test_str_and_repr():
    for repr_str, zulu_str, iso_str, iso_str_full in samples:
        z = eval(repr_str)
        assert z.__repr__() == repr_str
        assert str(z) == zulu_str
        assert z.str.seed == zulu_str
        assert z.iso() == iso_str
        assert Zulu.is_seed(zulu_str)
        assert Zulu.is_iso(iso_str)
        assert Zulu.is_iso(iso_str_full)
        assert Zulu.from_iso(iso_str) == Zulu.from_iso(iso_str_full)
        assert z.iso() == iso_str
        assert z.iso(full=True) == iso_str_full


def test_instantiate_string():
    for repr_str, zulu_str, iso_str, iso_str_full in samples:
        with pytest.raises(ZuluError):
            Zulu(zulu_str)
        with pytest.raises(ZuluError):
            Zulu(iso_str)

        z = eval(repr_str)
        assert Zulu.from_seed(zulu_str) == z
        assert Zulu.from_iso(iso_str) == z


def test_from_epoch():
    ts = datetime.datetime.now()
    ts = ts.replace(tzinfo=pytz.UTC)
    epoch = ts.timestamp()
    z = Zulu.from_epoch(epoch)
    assert ts.year == z.year
    assert ts.month == z.month
    assert ts.hour == z.hour
    assert ts.second == z.second
    assert epoch == z.epoch()


def test_local():
    a = Zulu()
    b = a.to_local()
    assert not isinstance(b, Zulu)
    c = Zulu.from_unaware_local(b.replace(tzinfo=None))
    d = c.to_local()
    assert b.hour == d.hour


def test_range():
    r = [Zulu(2020, 1, 1),
         Zulu(2020, 1, 2),
         Zulu(2020, 1, 3)]
    delta = Zulu.delta(days=1)
    ra = Zulu.range(Zulu(2020, 1, 1), n=3, delta=delta)
    assert r == ra


def test_delta():
    z1 = Zulu(2020, 1, 1)
    z2 = Zulu.glass(z1 + Zulu.delta(days=2))
    z3 = Zulu(2020, 1, 3)
    assert z3 == z2

    d1 = Zulu.delta(days=1)
    d2 = Zulu.delta(hours=1)
    # TODO: Consider skipping this test for python 3.6 and 3.7 to keep compatibility
    # since this is not that important..
    assert d1 + d2 == Zulu.delta(hours=1, days=1)


def test_from_seed():
    seed = '20200106T073022u399776Z'
    z2 = Zulu(2020, 1, 6, 7, 30, 22, 399776)
    z = Zulu.from_seed(seed)
    assert z == z2
    assert str(z) == seed
    assert z.seed() == seed


def test_find_seed():
    z1 = Zulu(2020, 1, 1)
    z2 = Zulu.now()
    z3 = Zulu(1989, 3, 14, 5, 5, 5)
    text = f'A text {z1} with {z2} more than one {z3} seed within'
    with pytest.raises(BadSeed):
        Zulu.find_seed(text)

    text = f'A text with no stub at all'
    with pytest.raises(BadSeed):
        Zulu.find_seed(text)

    for z in [z1, z2, z3]:
        text = f'A text with {z} one seed within'
        res = Zulu.find_seed(text)
        assert res == z


def test_find_seeds():
    z1 = Zulu(2020, 1, 1)
    z2 = Zulu.now()
    z3 = Zulu(1989, 3, 14, 5, 5, 5)
    text = f'A text {z1} with {z2} more than one {z3} seed within'
    zs = Zulu.find_seeds(text)
    assert len(zs) == 3
    assert (z1, z2, z3) == tuple(zs)


def test_parse_isostr():
    stub = '20200106T073022u399776Z'
    z = Zulu.from_seed(stub)
    isostr = '2020-01-06T07:30:22.399776+00:00'
    assert Zulu.is_iso(isostr)
    assert z.iso() == isostr
    z = Zulu.from_iso(isostr)
    assert z.iso() == isostr
    assert str(z) == stub

    z = Zulu.now()
    isostr = z.iso()
    z2 = Zulu.from_iso(isostr)
    assert z2 == z


def test_zulu_strings():
    z = Zulu(2019, 5, 5)
    assert str(z) == '20190505T000000u000000Z'
    z = Zulu(2019, 5, 5, 6, 6, 7)
    assert str(z) == '20190505T060607u000000Z'
    z = Zulu(2019, 5, 5, 1, 1, 1, 23342, pytz.utc)
    assert str(z) == '20190505T010101u023342Z'
    assert z.microsecond == 23342
    assert z.str.microsecond == '023342'

    z = Zulu(2019, 5, 5)
    assert z.year == 2019
    assert z.day == 5
    assert z.str.day == '05'
    assert z.str.second == '00'
    assert z.str.date == '20190505'
    assert z.str.time == '000000'
    assert z.str.seed == '20190505T000000u000000Z'


def test_elf():
    z = Zulu.elf(2020)
    assert z == Zulu(2020, 1, 1)

    z = Zulu.elf(2020, 2)
    assert z == Zulu(2020, 2, 1)

    z = Zulu.elf(2020, 2, 3)
    assert z == Zulu(2020, 2, 3)

    ts = datetime.datetime.now()
    z = Zulu.elf(ts, tz='utc')
    ts2 = ts.replace(tzinfo=pytz.UTC)
    z2 = Zulu(ts2)
    assert z == z2

    z = Zulu()
    ep = z.epoch()
    assert Zulu.from_epoch(ep) == z

    z = Zulu()
    assert Zulu.elf(z.iso()) == z
    assert Zulu.elf(z.seed()) == z


def test_zulu_elf_old():
    # Keep old tester for elf (the more the merrier)
    z = Zulu(2020, 5, 23, 11, 22, 33)
    zstr = z.seed()
    isostr = z.iso()
    dt = z.to_local()
    dtt = z.to_tz('Pacific/Easter')

    assert str(z) == str(Zulu.from_seed(zstr))
    assert str(z) == str(Zulu.from_iso(isostr))
    assert str(z) == str(Zulu(dt))
    assert str(z) == str(Zulu(dtt))

    assert str(z) == str(Zulu.elf(zstr))
    assert str(z) == str(Zulu.elf(isostr))
    assert str(z) == str(Zulu.elf(dt))
    assert str(z) == str(Zulu.elf(dtt))

    dt = dt.replace(tzinfo=None)
    assert str(z) == str(Zulu.elf(dt, tz='local'))
    dtz = z.to_tz('utc').replace(tzinfo=None)
    assert str(z) == str(Zulu.elf(dtz, tz='utc'))
    assert str(z) == str(Zulu.elf(z))


def test_glass():
    zulu = Zulu()
    assert Zulu.glass(zulu.seed()) == zulu
    assert Zulu.glass(zulu.iso()) == zulu
    assert Zulu.glass(zulu) == zulu
    assert Zulu.glass(2020, 1, 1) == Zulu(2020, 1, 1)


def test_round():
    zulu = Zulu(2020, 1, 1, 0, 0, 0)
    assert zulu.round_to_ms() == zulu
    assert zulu.floor_to_ms() == zulu
    assert zulu.round_to_s() == zulu
    assert zulu.floor_to_s() == zulu

    zulu = Zulu(2020, 1, 1, 0, 0, 0, 500)
    assert zulu.round_to_ms() == Zulu(2020, 1, 1, 0, 0, 0, 1000)
    assert zulu.floor_to_ms() == Zulu(2020, 1, 1, 0, 0, 0, 0)
    assert zulu.round_to_s() == Zulu(2020, 1, 1, 0, 0, 0)
    assert zulu.floor_to_s() == Zulu(2020, 1, 1, 0, 0, 0)

    zulu = Zulu(2020, 1, 1, 0, 0, 0, 499)
    assert zulu.round_to_ms() == Zulu(2020, 1, 1, 0, 0, 0)
    assert zulu.floor_to_ms() == Zulu(2020, 1, 1, 0, 0, 0, 0)
    assert zulu.round_to_s() == Zulu(2020, 1, 1, 0, 0, 0)
    assert zulu.floor_to_s() == Zulu(2020, 1, 1, 0, 0, 0)

    zulu = Zulu(2020, 1, 1, 0, 0, 0, 50000)
    assert zulu.round_to_ms() == zulu
    assert zulu.floor_to_ms() == zulu
    assert zulu.round_to_s() == Zulu(2020, 1, 1, 0, 0, 1)
    assert zulu.floor_to_s() == Zulu(2020, 1, 1, 0, 0, 0)

    zulu = Zulu(2020, 1, 1, 0, 0, 0, 49999)
    assert zulu.round_to_ms() == Zulu(2020, 1, 1, 0, 0, 0, 50000)
    assert zulu.floor_to_ms() == Zulu(2020, 1, 1, 0, 0, 0, 49000)
    assert zulu.round_to_s() == Zulu(2020, 1, 1, 0, 0, 0)
    assert zulu.floor_to_s() == Zulu(2020, 1, 1, 0, 0, 0)
