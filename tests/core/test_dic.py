import pytest

from mjooln import *


@pytest.fixture()
def dic1():
    return Dic(a=3,
               b=5.5,
               c='text')


@pytest.fixture()
def dic2(dic1):
    return Dic(a=3,
               b=5.5,
               c='text',
               d=dic1)


def test_dic1(dic1):
    assert str(dic1) == 'Dic(a=3, b=5.5, c=\'text\')'
    assert dic1.__repr__() == 'Dic(a=3, b=5.5, c=\'text\')'
    dic = dic1.to_dict()
    dic1b = dic1.from_dict(dic)
    assert dic1b.to_dict() == dic1.to_dict()


def test_dic2(dic2):
    r = 'Dic(a=3, b=5.5, c=\'text\', d=Dic(a=3, b=5.5, c=\'text\'))'
    assert dic2.__repr__() == r
    assert str(dic2) == r


def test_dic_add():
    d = Dic()
    d.a = 3
    d.b = 4
    assert d.to_dict() == {'a': 3, 'b': 4}
    d = d.to_dict()
    dd = Dic()
    dd.add(d)
    assert dd.to_dict() == d
    assert dd.a == 3
    assert dd.b == 4


def test_ignore_startswith():
    d = Dic(b=4, c=6)
    d._a = 'no'
    assert d.to_dict() == {'b': 4, 'c': 6}
    assert d._a == 'no'


def test_exists_only():
    d = Dic(a=2, b=3, c=4)
    d.add_only_existing({'b': 5, 'bb': 6})
    assert d.to_dict() == {'a': 2, 'b': 5, 'c': 4}


def test_force_match():
    d = Dic()
    d.a = 2
    d.b = 3
    d.c = 4
    d.force_equal({'a': 5, 'b': 6})
    assert d.to_dict() == {'a': 5, 'b': 6}


def test_flatten():
    d = {
        'one': {
            'two': 2,
            'three': 3,
        },
        'four': 4,
        'five': 5,
    }
    dic = Dic(d)
    fdic = dic.to_flat()
    tfdic = {'one__two': 2, 'one__three': 3, 'four': 4, 'five': 5}
    assert fdic == tfdic
    fdic = dic.to_flat(sep='/')
    tfdic = {'one/two': 2, 'one/three': 3, 'four': 4, 'five': 5}
    assert fdic == tfdic


def test_unflatten():
    d = {
        'one': {
            'two': 2,
            'three': 3,
            'four': {
                'five': 5,
                'six': 6,

            },
        },
        'seven': 7,
        'eight': 8,
    }
    dic = Dic(d)
    fdic = dic.to_flat()
    ndic = Dic.from_flat(fdic)
    assert ndic.to_dict() == d
