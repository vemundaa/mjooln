import pytest
import mjooln as mj
import random
import string


@pytest.fixture()
def error():
    class TestError(Exception):
        pass

    return TestError


@pytest.fixture()
def random_string(num_chars=1000):
    yield ''.join(random.choices(string.ascii_uppercase + string.digits + '\n',
                                 k=num_chars))


@pytest.fixture()
def dic():
    return {
        'zulu': mj.Zulu(),
        'text': 'Some very good text',
        'number': 34,
        'float': 3333.3333,
    }


def dev_create_test_files(files, num_chars=1000):
    import random
    import string
    for file in files:
        text = ''.join(random.choices(string.ascii_uppercase + string.digits + '\n',
                                      k=num_chars))
        file.write(text)
    return files


@pytest.fixture()
def tmp_folder():
    folder = mj.Folder.home().append('.mjooln_tmp_test_2SgTkk')
    folder.touch()
    folder.empty(name=folder.name())
    yield folder
    folder.empty(name=folder.name())
    folder.remove()


@pytest.fixture()
def tmp_files(tmp_folder):
    files = [mj.File.join(tmp_folder, f'test_{i}.txt') for i in range(3)]
    dev_create_test_files(files)
    yield files
    for file in files:
        file.delete(missing_ok=True)


@pytest.fixture()
def several_tmp_files(tmp_folder):
    files = [mj.File.join(tmp_folder, f'test_{i}.txt') for i in range(30)]
    dev_create_test_files(files)
    yield files
    for file in files:
        file.delete(missing_ok=True)


@pytest.fixture()
def several_atoms():
    zulus = mj.Zulu.range(n=30)
    return [mj.Atom(key='test_files__lots', zulu=x) for x in zulus]


@pytest.fixture()
def several_atom_files(tmp_folder):
    zulus = mj.Zulu.range(n=30)
    atom = [mj.Atom(key='test_files__lots', zulu=x) for x in zulus]
    files = [mj.File.join(tmp_folder, f'{s}.txt') for s in atom]
    dev_create_test_files(files)
    yield files
    for file in files:
        file.delete(missing_ok=True)


@pytest.fixture()
def trunk_keys():
    keys = ('test_zxw1cn4_trunk_1',
            'test_zxw1cn4_trunk_2',
            'test_zxw1cn4_trunk_3')
    for key in keys:
        mj.Cave.delete(key, ignore_missing=True)
    yield keys
    for key in keys:
        mj.Cave.delete(key, ignore_missing=True)


@pytest.fixture()
def medium_tree(tmp_folder):
    zulus = mj.Zulu.range(n=500)
    atom = [mj.Atom(key='test_files__heaps', zulu=x) for x in zulus]
    files = [mj.File.join(tmp_folder, f'{s}.txt') for s in atom]
    for file in files:
        file.touch()
    # dev_create_test_files(files)
    trunk = mj.Tree.Trunk(extension='txt',
                          key_level=2,
                          date_level=3)
    tree = mj.Tree.plant(tmp_folder, mj.Key('file_heap'), trunk)
    for file in files:
        tree.grow(file)
    for file in files:
        new_name = file.name().replace('test_files__heaps', 'test_files__many')
        file = file.rename(new_name)
        tree.grow(file, delete_native=True)
    yield tree
    tree.uproot(with_force=True, key=tree.atom.__key, identity=tree.atom.__identity)
    tmp_folder.empty(name=tmp_folder.name())
