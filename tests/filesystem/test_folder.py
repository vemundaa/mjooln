import pytest

from mjooln import *


def test_touch(tmp_folder):
    new_folder = tmp_folder.append('new_folder')
    assert not new_folder.exists()
    with pytest.raises(PathError):
        new_folder.is_file()
    with pytest.raises(PathError):
        new_folder.is_folder()
    new_folder.touch()
    assert new_folder.exists()
    assert not new_folder.is_file()
    assert new_folder.is_folder()
    assert new_folder.name() == 'new_folder'

    new_folder.untouch()
    assert not new_folder.exists()


def test_create_folder(tmp_folder):
    assert tmp_folder.is_empty()
    folder = tmp_folder.append('new_folder')
    folder.create()
    assert len(tmp_folder.list()) == 1
    folder.remove()
    assert tmp_folder.is_empty()


def test_append(tmp_folder):
    f = tmp_folder
    assert f.append('a') == str(f) + '/a'
    assert f.append('a', 'b') == str(f) + '/a/b'
    assert f.append('a', 'b', 'c') == str(f) + '/a/b/c'
    assert f.append(['a']) == str(f) + '/a'
    assert f.append(['a', 'b']) == str(f) + '/a/b'
    assert f.append(['a', 'b', 'c']) == str(f) + '/a/b/c'


def test_append_word_and_key(tmp_folder):
    f = tmp_folder
    assert f.append(Word('a')) == str(f) + '/a'
    assert f.append(Key('a__b')) == str(f) + '/a/b'
    assert f.append(Key('a__b__c')) == str(f) + '/a/b/c'


def test_div_and_add(tmp_folder):
    f = str(tmp_folder)
    fo = Folder(f + '/target/folder')
    fi = File(f + '/target/folder/target_file.txt')
    assert tmp_folder / 'target' / 'folder' == fo
    assert tmp_folder / 'target' / 'folder' * 'target_file.txt' == fi


def test_sub(tmp_folder):
    branch = 'some/sub/folders'
    fo = tmp_folder.append(branch)
    assert fo - tmp_folder == branch
    assert fo - fo == ''
    assert tmp_folder - tmp_folder == ''
    assert tmp_folder.append(fo - tmp_folder) == fo
    assert tmp_folder / branch == fo


def test_list(tmp_folder):
    folder = tmp_folder
    f1 = folder.append('f1')
    assert len(folder.list()) == 0
    f1.touch()
    assert len(folder.list()) == 1
    file = File.join(folder, 'dummy.txt')
    file.write('test')
    assert len(folder.list()) == 2
    assert len(folder.list('*.txt')) == 1
    f2 = folder.append('f2')
    assert len(folder.list()) == 2
    f2.create()
    assert len(folder.list()) == 3
    with pytest.raises(FolderError):
        f2.create()


def test_list_and_walk_and_disk_usage(tmp_folder, random_string):
    fi_count = 0
    fo_count = 0
    fi_size = 0
    for i in range(10):
        fo = tmp_folder.append(f'folder_{fo_count}')
        fo.touch()
        fo_count += 1
        for j in range(7):
            fi = fo.file(f'file_{fi_count}')
            fi.touch()
            fi_count += 1
            fi.write(random_string)
            fi_size += fi.size()
        for k in range(11):
            foo = fo.append(f'folder_{fo_count}')
            foo.touch()
            fo_count += 1
            for ll in range(12):
                fii = foo.file(f'file_{fi_count}')
                fii.touch()
                fi_count += 1
                fii.write(random_string)
                fi_size += fii.size()

    assert len(tmp_folder.list(recursive=True)) == fi_count + fo_count

    walk_all = list(tmp_folder.walk(include_files=True,
                                    include_folders=True))
    assert len(walk_all) == fi_count + fo_count

    assert len(list(tmp_folder.files())) == fi_count
    assert len(list(tmp_folder.folders())) == fo_count

    assert tmp_folder.disk_usage(include_folders=False,
                                 include_files=True) == fi_size


def test_empty_folder(tmp_folder):
    fo = tmp_folder.append('some_folder')
    fi = fo.file('content1.txt')
    fi.write('Valuable stuff')
    fi = fo.file('content2.txt')
    fi.write('More valuable stuff')
    assert not fo.is_empty()
    with pytest.raises(TypeError):
        fo.empty()
    assert len(fo.list()) == 2
    with pytest.raises(FolderError):
        fo.empty('wrong_name')
    assert fo.name() == 'some_folder'
    fo.empty('some_folder')
    assert len(fo.list()) == 0
    assert fo.exists()


def test_remove_empty_folders(tmp_folder):
    ifi = 0
    ifo = 0
    for i in range(10):
        fo = tmp_folder.append(f'fo_{ifo}')
        fo.touch()
        ifo += 1
        if i % 2 == 0:
            fi = fo.file(f'{ifi}.txt')
            fi.touch()
            ifi += 1
    fos = tmp_folder.list_folders()
    fos.sort()
    for i, fo in enumerate(fos[3:5]):
        for j in range(10):
            sfo = fo.append(f'sfo_{ifo}')
            sfo.touch()
            ifo += 1
            if j % 2 == 0:
                fi = sfo.file(f'{ifi}.txt')
                fi.touch()
                ifi += 1

    assert len(list(tmp_folder.folders())) == 30
    assert len(list(tmp_folder.files())) == 15

    tmp_folder.remove_empty_folders()

    assert len(list(tmp_folder.folders())) == 16
    assert len(list(tmp_folder.files())) == 15
