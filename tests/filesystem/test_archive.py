import pytest

from mjooln import *


def test_zip_to_gz(tmp_folder, tmp_files):
    num_files = len(tmp_files)
    # for file in tmp_files:
    #     file.delete()
    # content = [f'file_{x}' for x in range(num_files)]
    content = [f.read() for f in tmp_files]
    for f, c in zip(tmp_files, content):
        f.write(c)

    zip_files = []
    for file in tmp_files:
        zip_file = File.join(tmp_folder, file.stub() + '.zip')
        with zipfile.ZipFile(zip_file, 'w') as myzip:
            myzip.write(file)
        zip_files.append(zip_file)

    zfs = tmp_folder.list('*.zip')
    assert len(zfs) == num_files
    for z, c in zip(zip_files, content):
        zfile = File.glass(z)
        gzfile = Archive.zip_to_gz(zfile)
        cg = gzfile.read()
        assert cg == c

    gfs = tmp_folder.list('*.gz')
    assert len(gfs) == num_files

    assert tmp_folder.list('*.zip') == []
