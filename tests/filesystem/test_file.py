import pytest
import pandas as pd

from mjooln import *


def test_volume():
    assert Path('/').is_volume()


def test_extensions():
    pass


def test_folder(tmp_folder, tmp_files):
    _num_test_files = len(tmp_files)
    if PIXIE:
        with pytest.raises(PixieInPipeline):
            Folder(str(tmp_files[0]))
    folder = Folder.join(tmp_folder, 'new_folder')
    assert len(tmp_folder.list_files()) == _num_test_files
    folder.create()
    assert len(folder.list_files()) == 0
    assert len(tmp_folder.list()) == _num_test_files + 1
    assert len(tmp_folder.list_files()) == _num_test_files
    assert len(tmp_folder.list_folders()) == 1
    folder.remove()
    assert len(tmp_folder.list()) == _num_test_files
    assert len(tmp_folder.list_files()) == _num_test_files
    assert len(tmp_folder.list_folders()) == 0


def test_touch(tmp_folder):
    subfolder = tmp_folder.append('subfolder')
    file = subfolder.file('file_to_touch.txt')
    assert not subfolder.exists()
    assert not file.exists()
    file.touch()
    assert subfolder.exists()
    assert file.exists()
    assert file.size() == 0


def test_file_error(tmp_folder):
    folder = tmp_folder.append('new_folder')
    folder.create()
    if PIXIE:
        with pytest.raises(PixieInPipeline):
            File(str(folder))


def test_glass(tmp_folder):
    folder = tmp_folder.append('new_folder')
    folder.create()
    file = folder.file('somefile.txt')
    file_str = str(file)
    if PIXIE:
        with pytest.raises(PixieInPipeline):
            File(file)
    assert file == File.glass(file)
    assert file == File.glass(file_str)
    assert file == file.glass(file)
    assert file == file.glass(file_str)


def test_delete(tmp_folder, tmp_files):
    path_list = tmp_folder.list()
    num_files = len(path_list)
    for path in path_list:
        path = File.glass(path)
        if path.exists() and path.is_file():
            path.delete()
            num_files -= 1
        assert len(tmp_folder.list()) == num_files


def test_overwrite(tmp_folder, tmp_files):
    path_list = tmp_folder.list()
    fo = tmp_folder.append('dummy')
    fo.touch()
    for path in path_list:
        file = File.glass(path)
        file.move(file.folder(), overwrite=True)
        with pytest.raises(FileError):
            file.move(file.folder(), overwrite=False)
        with pytest.raises(FileError):
            file.copy(file.folder(), overwrite=True)
        with pytest.raises(FileError):
            file.copy(file.folder(), overwrite=False)
        file.copy(fo, overwrite=False)
        with pytest.raises(FileError):
            file.copy(fo, overwrite=False)
        file.copy(fo, overwrite=True)


def test_hidden():
    f = File('.something_hidden')
    assert f.is_hidden()


def test_stub():
    stub = 'something'
    variations = [
        f'{stub}',
        f'.{stub}',
        f'{stub}.txt',
        f'{stub}.txt.gz',
        f'{stub}.txt.gz.aes',
        f'.{stub}',
        f'.{stub}.txt',
        f'.{stub}.txt.gz',
        f'.{stub}.txt.gz.aes',
        f'..{stub}.txt.gz',
        f'...{stub}.txt.gz',
        f'..{stub}.very.odd.filename',
    ]
    for v in variations:
        f = File(v)
        assert f.stub() == stub


def test_extension():
    stub = 'something'
    variations = [
        {
            'file_name': f'.{stub}',
            'extension': None,
        },
        {
            'file_name': f'{stub}',
            'extension': None,
        },
        {
            'file_name': f'{stub}.gz',
            'extension': None,
        },
        {
            'file_name': f'{stub}.gz.aes',
            'extension': None,
        },
        {
            'file_name': f'{stub}.txt',
            'extension': 'txt',
        },
        {
            'file_name': f'{stub}.txt.gz',
            'extension': 'txt',
        },
        {
            'file_name': f'{stub}.txt.gz.aes',
            'extension': 'txt',
        },
    ]
    for v in variations:
        f = File(v['file_name'])
        assert f.extension() == v['extension']


def test_compression(tmp_folder, tmp_files):
    _num_test_files = len(tmp_files)
    assert len(tmp_folder.list('*.txt')) == _num_test_files
    tmp_file = tmp_files[0]
    size_before = tmp_file.size()
    extension_before = tmp_file.extension()
    assert extension_before == 'txt'
    assert not tmp_file.is_compressed()
    tmp_file.compress()
    assert len(tmp_folder.list('*.txt')) == _num_test_files - 1
    assert len(tmp_folder.list('*.txt.gz')) == 1
    size_after = tmp_file.size()
    assert tmp_file.is_compressed()
    assert size_after < size_before
    assert tmp_file.extension() == 'txt'
    tmp_file.decompress()
    assert not tmp_file.is_compressed()
    assert tmp_file.size() == size_before
    assert tmp_file.extension() == 'txt'

    assert len(tmp_folder.list('*.txt')) == _num_test_files


def test_copy(tmp_folder, tmp_files):
    _num_test_files = len(tmp_files)
    source_folder = tmp_folder
    assert len(source_folder.list('*.txt')) == _num_test_files
    copy_folder = tmp_folder.append('copy')
    copy_folder.create()
    copied = []
    for tmp_file in tmp_files[1:]:
        with pytest.raises(FileError):
            tmp_file.copy(tmp_folder)
        copy_file = tmp_file.copy(copy_folder)
        copied.append(copy_file)
    assert len(source_folder.list('*.txt')) == _num_test_files
    assert len(copy_folder.list('*.txt')) == _num_test_files - 1
    for file in tmp_files:
        file.delete()
    assert len(source_folder.list('*.txt')) == 0
    assert len(copy_folder.list('*.txt')) == _num_test_files - 1
    for file in copied:
        file.delete()
    assert len(source_folder.list('*.txt')) == 0
    assert len(copy_folder.list('*.txt')) == 0


def test_move(tmp_folder, tmp_files):
    _num_test_files = len(tmp_files)
    source_dir = tmp_folder
    move_dir = tmp_folder.append('move')
    assert len(source_dir.list('*.txt')) == _num_test_files
    moved = []
    for tmp_file in tmp_files[1:]:
        tmp_file.move(move_dir)
        moved.append(tmp_file)
    assert len(source_dir.list('*.txt')) == 1
    assert len(move_dir.list('*.txt')) == _num_test_files - 1
    for moved_file in moved:
        moved_file.move(source_dir)
    assert len(source_dir.list('*.txt')) == _num_test_files
    assert len(source_dir.list('*.txt.move')) == 0


def test_encrypt(tmp_folder, tmp_files):
    _num_test_files = len(tmp_files)
    tmp_dir = tmp_folder
    assert len(tmp_dir.list('*.txt')) == _num_test_files

    tmp_file = tmp_files[0]
    data_before = tmp_file.read()
    extension = tmp_file.extension()
    assert extension == 'txt'
    assert not tmp_file.is_encrypted()
    key = Crypt.generate_key()
    tmp_file.encrypt(crypt_key=key)
    assert tmp_file.extension() == 'txt'
    assert tmp_file.is_encrypted()

    tmp_file.decrypt(key)
    assert not tmp_file.is_compressed()
    assert tmp_file.extension() == 'txt'
    data_after = tmp_file.read()
    assert data_before == data_after

    tmp_file = tmp_files[1]
    data_before = tmp_file.read()
    extension = tmp_file.extension()
    assert extension == 'txt'
    assert not tmp_file.is_encrypted()
    salt = Crypt.salt()
    key = Crypt.key_from_password(salt, 'test')
    tmp_file.encrypt(key)
    assert tmp_file.extension() == 'txt'
    assert tmp_file.is_encrypted()

    key = Crypt.key_from_password(salt, 'test')
    tmp_file.decrypt(key)
    assert not tmp_file.is_compressed()
    assert tmp_file.extension() == 'txt'
    data_after = tmp_file.read()
    assert data_before == data_after


def test_read_write_encrypted(tmp_folder):
    text = 'This is a sample text, in normal string format.'
    f = File.join(tmp_folder, 'test.gz.aes')
    password = 'some password'
    crypt_key = Crypt.generate_key()

    with pytest.raises(FileError):
        f.write(text)
    f.write(text, password=password)
    assert len(tmp_folder.list()) == 1

    with pytest.raises(FileError):
        f.read(crypt_key=crypt_key, password=password)
    assert f.exists()
    with pytest.raises(FileError):
        f.read()
    assert f.exists()
    with pytest.raises(CryptError):
        f.read(crypt_key=crypt_key)
    assert f.exists()
    read_text = f.read(password=password)
    assert text == read_text
    assert len(tmp_folder.list()) == 1

    f.delete()
    f.write(text, crypt_key=crypt_key)
    assert len(tmp_folder.list()) == 1
    with pytest.raises(FileError):
        f.read(crypt_key=crypt_key, password=password)
    assert f.exists()
    with pytest.raises(FileError):
        f.read()
    assert f.exists()
    with pytest.raises(CryptError):
        f.read(password=password)
    assert f.exists()
    assert len(tmp_folder.list()) == 1
    read_text = f.read(crypt_key=crypt_key)
    assert text == read_text
    assert len(tmp_folder.list()) == 1


def test_json_file(tmp_folder, dic):
    f = File.join(tmp_folder, 'some_text.json')
    dic1 = dic.copy()
    dic1['zulu'] = dic1['zulu'].iso()
    f.write_json(dic1)
    dic2 = f.read_json()
    dic2['zulu'] = Zulu.from_iso(dic2['zulu'])
    assert dic == dic2

    f = File.join(tmp_folder, 'some_text.json.gz')
    dic1 = dic.copy()
    dic1['zulu'] = dic1['zulu'].iso()
    f.write_json(dic1)
    dic2 = f.read_json()
    dic2['zulu'] = Zulu.from_iso(dic2['zulu'])
    assert dic == dic2

    f = File.join(tmp_folder, 'some_text.json.gz.aes')
    dic1 = dic.copy()
    dic1['zulu'] = dic1['zulu'].iso()
    f.write_json(dic1, password='test')
    dic2 = f.read_json(password='test')
    dic2['zulu'] = Zulu.from_iso(dic2['zulu'])
    assert dic == dic2


def test_yaml_file(tmp_folder, dic):
    f = File.join(tmp_folder, 'some_text.json')
    dic1 = dic.copy()
    dic1['zulu'] = dic1['zulu'].iso()
    f.write_yaml(dic1)
    dic2 = f.read_yaml()
    dic2['zulu'] = Zulu.from_iso(dic2['zulu'])
    assert dic == dic2

    f = File.join(tmp_folder, 'some_text.json.gz')
    dic1 = dic.copy()
    dic1['zulu'] = dic1['zulu'].iso()
    f.write_yaml(dic1)
    dic2 = f.read_yaml()
    dic2['zulu'] = Zulu.from_iso(dic2['zulu'])
    assert dic == dic2

    f = File.join(tmp_folder, 'some_text.json.gz.aes')
    dic1 = dic.copy()
    dic1['zulu'] = dic1['zulu'].iso()
    f.write_yaml(dic1, password='test')
    dic2 = f.read_yaml(password='test')
    dic2['zulu'] = Zulu.from_iso(dic2['zulu'])
    assert dic == dic2


def test_read_lines(tmp_folder, tmp_files):
    print(tmp_folder)
    file = tmp_files[0]
    file.delete()
    with open(file, 'w') as f:
        f.writelines(['line1\n', 'line2\n', 'line3\n'])

    assert file.size() > 0
    lines = file.readlines(num_lines=2)
    assert lines[0] == 'line1'
    assert lines[1] == 'line2'
    assert len(lines) == 2

    lines = file.readlines(num_lines=1)
    assert isinstance(lines, str)
    assert lines == 'line1'

    file.compress()
    lines = file.readlines(num_lines=2)
    assert lines[0] == 'line1'
    assert lines[1] == 'line2'

    file.decompress()
    assert file.size() > 0
    lines = file.readlines(num_lines=2)
    assert lines[0] == 'line1'
    assert isinstance(lines[0], str)
    assert lines[1] == 'line2'
    assert isinstance(lines[1], str)


def test_pandas_compatibility(tmp_folder):
    # Make sure nothing in the File object interfers with pandas file handling
    df = pd.DataFrame(columns=['a', 'b'], data=[[1, 2], [3, 4]])
    fi = tmp_folder.file('test.csv')
    df.to_csv(fi)
    df2 = pd.read_csv(fi, index_col=0)
    assert df.equals(df2)

    df = pd.DataFrame(columns=['a', 'b', 'c'], data=[[1, 2, 3], [4, 5, 6]])
    fi = tmp_folder.file('test.csv')
    df.to_csv(fi)
    fi.compress()
    df2 = pd.read_csv(fi, index_col=0)
    assert df.equals(df2)
