import pytest

from mjooln import *

_num_items = 3


# TODO: Add check for is_empty, is_null and delete, with exceptions


def _make(folder, subfolder, name, is_folder, exists):
    paths = []
    for i in range(_num_items):
        if subfolder:
            folder = os.path.join(folder, subfolder)
        else:
            folder = folder
        path = os.path.join(folder, name.format(i))
        if exists and is_folder:
            if not os.path.exists(path):
                os.makedirs(path)
        if exists and not is_folder:
            if not os.path.exists(folder):
                os.makedirs(folder)
            with open(path, 'w') as f:
                f.write('Some simple text')
        paths.append(Path(path))
    return paths


@pytest.fixture()
def paths(tmp_folder):
    paths = {}
    paths['files'] = _make(tmp_folder, '', 'file_{}.txt', False, True)
    paths['not_files'] = _make(tmp_folder, '', 'not_file_{}.txt', False, False)
    paths['folders'] = _make(tmp_folder, '', 'folder_{}', True, True)
    paths['not_folders'] = _make(tmp_folder, '', 'not_folder_{}', True, False)
    subfolder = paths['folders'][0]
    for i in range(_num_items):
        paths['files_in_folder'] = _make(tmp_folder, subfolder,
                                         'file_in_folder_{}.txt', False, True)
        paths['not_files_in_folder'] = _make(tmp_folder, subfolder,
                                             'not_file_in_folder_{}.txt', False,
                                             False)
        paths['folders_in_folder'] = _make(tmp_folder, subfolder,
                                           'folder_in_folder_{}', True, True)
        paths['not_folders_in_folder'] = _make(tmp_folder, subfolder,
                                               'not_folder_in_folder_{}', True,
                                               False)

    yield paths


def test_slashes():
    assert Path('/') == '/'
    assert Path('\\') == '/'
    if Path.platform() == Path.WINDOWS:
        assert Path('C:\\dev\\dummy') == 'C:/dev/dummy'
        assert Path('C:/dev\\dummy') == 'C:/dev/dummy'
    else:
        assert Path('\\dev\\dummy') == '/dev/dummy'
        assert Path('/dev\\dummy') == '/dev/dummy'


def test_drives():
    if Path.platform() == Path.WINDOWS:
        assert Path('C:/dev/dummy').mountpoint() == 'C:'
    # TODO: Add these tests if changing drive handling
    # assert Path('/dev/dummy').drive() == ''
    # assert Path('dev/dummy').drive() == ''


def test_network_drive():
    nd = Path('\\\\some network drive')
    assert nd == '//some network drive'
    assert nd.is_network_drive()
    assert nd.on_network_drive()


def test_name():
    assert Path('this/is/a/path.txt').name() == 'path.txt'
    assert Path('another/path/here').name() == 'here'


def test_tilde():
    home = os.path.expanduser('~')
    home_path = Path(home)
    assert Path('~') == home_path

    sth_at_home = os.path.expanduser('~/somewhere/at_home.txt')
    sth_at_home_path = Path(sth_at_home)
    assert Path('~/somewhere/at_home.txt') == sth_at_home_path


def test_parts():
    if Path.platform() == Path.WINDOWS:
        assert Path('C:/dev/dummy').parts() == ['C:', 'dev', 'dummy']
    assert Path('/how/to/do/this').parts() == ['how', 'to', 'do', 'this']
    assert Path('/and\\odd/slashes').parts() == ['and', 'odd', 'slashes']


# def test_ending():
#     assert Path('def.txt').extensions() == ['txt']
#     assert Path('/help/me/something.txt.gz').extensions() == ['txt', 'gz']
#     with pytest.raises(PathError):
#         Path('file_with_odd_extensions.csv.odd.gz').extension()
#
#     assert Path('csvfile.csv').extension() == 'csv'
#     assert Path('csvfile.csv.gz').extension() == 'csv'
#     assert Path('csvfile.csv.aes').extension() == 'csv'
#     assert Path('csvfile.csv.gz.aes').extension() == 'csv'

# def test_clean(Paths):
#     assert len(glob.glob(os.Path.join(cfg.TESTDIR, '*'))) > 0
#     Path(cfg.TESTDIR).empty()
#     assert len(glob.glob(os.Path.join(cfg.TESTDIR, '*'))) == 0


def test_exists_and_is_type(paths):
    for f in paths['not_files']:
        p = Path.glass(f)
        assert not p.exists()
        with pytest.raises(PathError):
            p.is_file()
        with pytest.raises(PathError):
            p.is_folder()

    for f in paths['not_folders']:
        p = Path.glass(f)
        assert not p.exists()
        with pytest.raises(PathError):
            p.is_file()
        with pytest.raises(PathError):
            p.is_folder()

    for f in paths['files']:
        p = Path.glass(f)
        assert p.exists()
        assert p.is_file()
        assert not p.is_folder()

    for f in paths['folders']:
        p = Path.glass(f)
        assert p.exists()
        assert not p.is_file()
        assert p.is_folder()

    for f in paths['not_files_in_folder']:
        p = Path.glass(f)
        assert not p.exists()
        with pytest.raises(PathError):
            p.is_file()
        with pytest.raises(PathError):
            p.is_folder()

    for f in paths['not_folders_in_folder']:
        p = Path.glass(f)
        assert not p.exists()
        with pytest.raises(PathError):
            p.is_file()
        with pytest.raises(PathError):
            p.is_folder()

    for f in paths['files_in_folder']:
        p = Path.glass(f)
        assert p.exists()
        assert p.is_file()
        assert not p.is_folder()

    for f in paths['folders_in_folder']:
        p = Path.glass(f)
        assert p.exists()
        assert not p.is_file()
        assert p.is_folder()


def test_created_and_modified(paths):
    for f in paths['files'] + paths['folders']:
        c = f.created()
        m = f.modified()
        n = Zulu.now()
        assert n > c
        assert n - c < Zulu.delta(seconds=5)
        assert n > m
        assert n - m < Zulu.delta(seconds=5)
