from mjooln.experimental import *

def test_load_and_save():
    app = App()
    app.save()
    app2 = app.load()
    assert app.to_doc() == app2.to_doc()
    fi = app._store.file(app._key())
    assert fi.exists()
    fi.delete()
    assert not fi.exists()

