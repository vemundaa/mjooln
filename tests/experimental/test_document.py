import pytest

from mjooln import *
from mjooln.experimental import *


def test_save_and_load_json(tmp_folder):
    file = tmp_folder.file('my_docfile.json')
    with pytest.raises(FileError):
        Document.load(file)
    dofi = Document(file, 'test', one=1, two=2, three='four')
    dofi.save()
    dofi2 = Document.load(file)
    assert dofi == dofi2

def test_save_and_load_json_human(tmp_folder):
    file = tmp_folder.file('my_docfile.json')
    dofi = Document(file, 'test', one=1, two=2, three='four')
    dofi.save(human=True)
    dofi2 = Document.load(file)
    assert dofi == dofi2



def test_save_and_load_yaml(tmp_folder):
    file = tmp_folder.file('my_docfile.yaml')
    with pytest.raises(FileError):
        Document.load(file)
    dofi = Document(file, 'test', one=1, two=2, three='four')
    dofi.save()
    dofi2 = Document.load(file)
    assert dofi == dofi2


def test_invalid_extension(tmp_folder):
    file = tmp_folder.file('my_docfile.txt')
    if PIXIE:
        with pytest.raises(PixieInPipeline):
            Document(file, 'test', one=1, two=2, three='four')
    else:
        dofi = Document(file, 'test', one=1, two=2, three='four')
        with pytest.raises(DocumentError):
            dofi.save()


def test_atom(tmp_folder):
    file = tmp_folder.file('my_docfile.json')
    atom = Atom('test_me')
    dofi = Document(file, atom, one=1, two=2, three='four')
    dofi.save()
    dofi2 = Document.load(file)
    assert dofi == dofi2
    assert dofi.atom == atom
    assert dofi2.atom == atom


def test_timestamps(tmp_folder):
    file = tmp_folder.file('my_docfile.json')
    dofi = Document(file, 'test', one=1, two=2, three='four')
    dofi.save()
    assert dofi.created() == dofi.modified()
    assert dofi.created() < Zulu.now()
    dofi.one = 44
    dofi.save()
    assert dofi.modified() < dofi.created()


def test_modify(tmp_folder):
    file = tmp_folder.file('my_docfile.json')
    dofi = Document(file, 'test', one=1, two=2, three='four')
    dofi.save()
    dofi2 = Document.load(file)
    dofi2.one = 4
    dofi2.save()
    dofi3 = Document.load(file)
    assert dofi3 != dofi
    assert dofi3.two == dofi.two
    assert dofi3.three == dofi.three


def test_save_and_load_compressed(tmp_folder):
    file = tmp_folder.file('my_docfile.json.gz')
    dofi = Document(file, 'test', one=1, two=2, three='four')
    dofi.save(human=True)
    dofi2 = Document.load(file)
    assert dofi == dofi2


def test_save_and_load_encrypted(tmp_folder):
    file = tmp_folder.file('my_docfile.json.gz.aes')
    dofi = Document(file, 'test', one=1, two=2, three='four')
    crypt_key = Crypt.generate_key()
    dofi.save(human=True, crypt_key=crypt_key)
    with pytest.raises(FileError):
        Document.load(file)
    dofi2 = Document.load(file, crypt_key=crypt_key)
    assert dofi == dofi2