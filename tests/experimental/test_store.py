import pytest

from mjooln import *
from mjooln.experimental import *

HASH = 'ai0cv6'


def get_key(key_str):
    return Key(HASH + '__' + str(key_str))


def test_text():
    key = get_key('dummy_data')
    data = 'some text to be put in a key'
    Store._is_encrypted = False
    Store.put(key, data)
    assert Store.get(key) == data


def test_text_encrypted():
    key = get_key('dummy_data')
    data = 'some text to be put in a key'
    Store._is_encrypted = True
    with pytest.raises(FileError):
        Store.put(key, data)
    Store.put(key, data, password='123')
    with pytest.raises(FileError):
        Store.get(key)
    assert Store.get(key, password='123') == data


def test_docstore():
    key = get_key('dummy_data')
    data = {
        'a': 3,
        'b': 55.5,
        'c': 'some text',
    }
    Store._is_encrypted = False
    DocStore.put(key, data)
    assert DocStore.get(key) == data


def test_docstore_encrypted():
    key = get_key('dummy_data')
    data = {
        'a': 3,
        'b': 55.5,
        'c': 'some text',
    }
    Store._is_encrypted = True
    with pytest.raises(FileError):
        DocStore.put(key, data)
    DocStore.put(key, data, password='123')
    with pytest.raises(FileError):
        DocStore.get(key)
    assert DocStore.get(key, password='123') == data


def test_configstore():
    key = get_key('dummy_data')
    data = {
        'a': 3,
        'b': 55.5,
        'c': 'some text',
    }
    Store._is_encrypted = False
    ConfigStore.put(key, data)
    assert ConfigStore.get(key) == data


# Cleanup test folder
fo = Store._folder.append(HASH)
if fo.exists():
    fo.empty(fo.name())
    fo.remove()
