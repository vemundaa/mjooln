ruff:
	poetry install --with dev
	poetry run ruff format mjooln --fix
	
mypy:
	poetry run mypy mjooln --check-untyped-defs

megalinter:
	poetry export --without-hashes --format=requirements.txt > requirements.txt
	mega-linter-runner