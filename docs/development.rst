.. _development:


Development
===========

Reciepe for setting up the ``mjooln`` development environment. 
If you're lucky, this works out of the box

.. note::
    Last updated: 2024-08-31

.. note::
    All examples are for Mac

XCode command line tools
------------------------

Make sure you have an updated version of
`Xcode command line tools <https://mac.install.guide/commandlinetools/2>`_

PyEnv
---------------

Make sure required tools are installed with homebrew

.. code-block:: bash

    brew install readline ncurses

Use homebrew to install pyenv

.. code-block:: bash

    brew install pyenv

Add the following to your ``.zshrc``

.. code-block:: bash

    export PYENV_ROOT="$HOME/.pyenv"
    command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"

Python
----------------

Use pyenv to install the versions you want

.. code-block:: text

    pyenv install 3.10
    pyenv install 3.11
    pyenv install 3.12

Select the python version to use globally

.. code-block:: text

    pyenv global 3.10

To see all versions and which one is selected

.. code-block:: text

    pyenv versions

To use a specific version in a specific repository, go to the folder and use

.. code-block:: text

    pyenv local 3.11

pipx
--------------

.. code-block:: text

    brew install pipx
    pipx ensurepath

Alternatively, use the `official documentation <https://pipx.pypa.io/stable/installation/>`_

Poetry
------

Use pipx to install poetry

.. code-block:: text

    pipx install poetry

Alternatively, use the `official documentation <https://python-poetry.org/docs/>`_

Requirements
^^^^^^^^^^^^

After cloning the repo, install requirements with poetry

.. code-block:: text

    poetry install

To include test or docs requirements use ``--with``

.. code-block:: text

    poetry install --with tests
    poetry install --with docs


Megalinter
----------

Follow the documentation at `megalinter.io <https://megalinter.io/v5.0.4/installation/>`_:

.. code-block::

    npx mega-linter-runner --install


If the command ``npx`` doesn't work, you need to install node.js first

.. code-block:: text

    brew install node

You also need docker installed (instructions are for Docker Desktop):

.. code-block::
    
    brew install --cask docker

.. note::

    ``--cask`` means a GUI application. The alternative is formulae
