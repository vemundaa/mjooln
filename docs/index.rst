=====================
mjooln Documentation
=====================

.. sidebar:: About mjooln

    mjooln [`no: "mjøln"; en: "myawln"`] is a file handling toolbox for Microservice Developers and
    Data Scientists working in Python


    * **Source code**: `mjooln <https://gitlab.com/vemundaa/mjooln>`_

Getting started
---------------
.. toctree::
   :maxdepth: 1

   what_is_mjooln
   installation
   examples
   development
   links
   

API
---

.. toctree::
    :maxdepth: 1

    api

References
----------

.. toctree::
   :maxdepth: 1

   references

Changelog
---------

.. toctree::
    :maxdepth: 2

    changelog

