.. _installation:


Installation
============

.. sidebar:: Updated

    2024-08-31
    
Requires Python 3.9 or later:

.. code-block:: console

    $ pip install mjooln

For latest available version see `PyPi <https://pypi.org/project/mjooln/>`_