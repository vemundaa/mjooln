
Snippets
========

.. note:: To be continued...

The following import is assumed in all examples

.. code-block:: python

    import mjooln as mj

Read text from file
-------------------

.. code-block:: python

    contents = mj.File('/path/to/file/my_file.txt').read()

Write text to file
------------------

.. code-block:: python

    mj.File('/path/to/file/my_file.txt').write(contents)


Write dictionary to JSON file
-----------------------------
.. code-block:: python

    mj.File('/path/to/file/my_dictionary.json').write_json(di)



