References
==========

Documentation strongly aided by following
`locust <https://docs.locust.io/en/stable/index.html>`_
by example

Many a snippet copied from
`stack overflow <https://stackoverflow.com>`_
and similar sites

The best Python reference is
`The Hitchhiker's Guide to Python <https://docs.python-guide.org/>`_


