Changelog
=========

0.7.0
-----

Changed
^^^^^
Switched to ``poetry`` with ``pyproject.toml``, thus removing
requirements.txt and setup.py

0.6.3
-----

Changed
^^^^^
API documentation

Fixed
^^^^^
Missing extra in ``setup.cfg``


0.6.2
-----

Remove default import of package ``psutil`` for Windows installations

0.6.1
-----

Set PIXIE to ``False`` by default

0.6.0
-----

Started changelog