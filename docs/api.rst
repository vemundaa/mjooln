.. _api:

API
###

Core
====

Atom
----------

.. autoclass:: mjooln.Atom
    :members: date_elements, element_count, time_elements, key_elements, elf,
        from_dict, from_doc, from_seed, identity, key, to_dict, to_doc,
        with_sep, zulu

Crypt
-----------

.. autoclass:: mjooln.Crypt
    :members: decrypt, encrypt, generate_key, key_from_password, salt

Dic
---------

.. autoclass:: mjooln.Dic
    :members: add, add_only_existing, flatten, force_equal, from_dict,
        from_flat, keys, print, print_flat, to_dict, to_flat, unflatten

Doc
---------

.. autoclass:: mjooln.Doc
    :members: add_json, add_yaml, from_doc, from_json, from_yaml, to_doc,
        to_json, to_yaml

Identity
--------------

.. autoclass:: mjooln.Identity
    :members: elf, from_classic, from_compact, from_seed, is_classic,
        is_compact

JSON
----------

.. autoclass:: mjooln.JSON
    :members: dumps, loads


Key
---------

.. autoclass:: mjooln.Key
    :members: elf, verify_key, with_separator, words


Seed
----------

.. autoclass:: mjooln.Seed
    :members: find_seed, find_seeds, from_seed, is_seed, seed, seed_in,
        verify_seed


Waiter
------------

.. autoclass:: mjooln.Waiter
    :members: come, wait, sleep


Word
----------

.. autoclass:: mjooln.Word
    :members: check, elf, none, is_numeric, is_int, to_int,
        increment, index, is_none


YAML
----------

.. autoclass:: mjooln.YAML
    :members: dumps, loads

Zulu
----------

.. autoclass:: mjooln.Zulu
    :members: add, all_timezones, delta, elf, epoch, format, from_epoch,
        from_iso, from_seed, from_str, from_unaware, from_unaware_local,
        from_unaware_utc, is_iso, iso, now, parse, range, to_local, to_tz,
        to_unaware


File System
===========

File
----------

.. autoclass:: mjooln.File
    :members: read, write, compress, decompress, encrypt, decrypt, copy,
        delete, delete_if_exists, extension, extensions, folder, home,
        is_compressed, is_encrypted, is_hidden, md5_checksum, move,
        new, read_json, write_json, read_yaml, write_yaml, stub, touch,
        untouch


Folder
------------

.. autoclass:: mjooln.Folder
    :members: create, append, current, disk_usage, empty, file,
        files, folders, home, is_empty, list, list_files, list_folders,
        parent, print, remove, remove_empty_folders, touch, untouch,
        walk


Path
----------

.. autoclass:: mjooln.Path
    :members: as_file, as_folder, as_path, as_pure_path, created, modified,
        exists, has_valid_mountpoint, host, is_file, is_folder,
        is_network_drive, is_volume, join, listdir, mountpoints, name,
        network_drive, on_network_drive, parts, platform, raise_if_not_exists,
        size, validate, volume



Archive
-------------

.. autoclass:: mjooln.Archive
    :members: is_zip, zip_to_gz


Experimental
============

Document
--------------

.. autoclass:: mjooln.experimental.document.Document
    :members: created, delete, exists, file, from_doc, to_doc, load, save,
        modified

App
--------------

.. autoclass:: mjooln.experimental.app.App

Store
--------------

.. autoclass:: mjooln.experimental.store.Store

System
--------------

.. autoclass:: mjooln.experimental.system.System


Exceptions
==========

.. autoexception:: mjooln.MjoolnException
.. autoexception:: mjooln.CryptError
.. autoexception:: mjooln.BadSeed
.. autoexception:: mjooln.DicError
.. autoexception:: mjooln.DocError
.. autoexception:: mjooln.DocumentError
.. autoexception:: mjooln.IdentityError
.. autoexception:: mjooln.BadWord
.. autoexception:: mjooln.NotAnInteger
.. autoexception:: mjooln.ZuluError
.. autoexception:: mjooln.PathError
.. autoexception:: mjooln.FileError
.. autoexception:: mjooln.ArchiveError
.. autoexception:: mjooln.FolderError

