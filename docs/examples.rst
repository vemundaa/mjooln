.. _examples:

Examples
========

.. note:: This page is still work in progress,
    but there are some examples in the API docs

.. toctree::
   :maxdepth: 1

   examples_quick_start

.. toctree::
   :maxdepth: 1

   examples_snippets

.. toctree::
   :maxdepth: 1

   examples_advanced
