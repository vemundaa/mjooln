What is mjooln?
===============

mjooln [`no: "mjøln"; en: "myawln"`] is a file handling toolbox for Microservice Developers and Data
Scientists working in Python

Background
----------

Development was motivated by writing ``os.path.join()`` one time too many,
the usefulness of simplified read/write of encrypted configuration files
and the never ending issue of datetime and timezones

Also, despite Data Lakes, Delta Lakes, Lakehouses
and Object Stores, there always seems to be some stage where a lot of csv-files
have to be compressed and stored in a decent folder structure in the local
filesystem


Overview
--------

:class:`.File` and :class:`.Folder` facilitate file and folder handling,
as well as read/write -- with or without compression/encryption

:class:`.Zulu` extends ``datetime.datetime`` with convenience methods, and
is timezone aware and *always* UTC

:class:`.Dic` mirrors object attributes to/from a dictionary, while
:class:`.Doc` mirrors object attributes to/from JSON and YAML
